package com.njp.post;

import com.njp.AppConstants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@RestController
public class ImageController {

    @PostMapping("/upload")
    public ResponseEntity addImage(@RequestParam("image") MultipartFile file) {
        String fileName;
        if (file.isEmpty()) {
            return ResponseEntity.badRequest().build();
        } else {
            try {
                var bytes = file.getBytes();
                fileName = UUID.randomUUID().toString() + ".png";
                var path = Paths.get(AppConstants.IMAGES_ROOT + fileName);
                Files.write(path, bytes);
            } catch (IOException e) {
                e.printStackTrace();
                return ResponseEntity
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .build();
            }
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(new Response(fileName));
        }
    }


    static class Response {
        public String imageId;

        public Response(String imageId) {
            this.imageId = imageId;
        }
    }
}
