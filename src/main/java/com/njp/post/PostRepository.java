package com.njp.post;

import com.njp.user.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    public List<Post> findByPostedBy_FollowersContains(AppUser user);


    @Query("SELECT p FROM Post AS p WHERE :user MEMBER OF p.postedBy.followers")
    public List<Post> findPostsForUser(@Param("user") AppUser user);
}
