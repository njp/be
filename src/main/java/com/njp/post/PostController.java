package com.njp.post;

import com.njp.post.comments.Comment;
import com.njp.post.comments.CommentRepository;
import com.njp.security.AppUserDetailsService;
import com.njp.user.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/posts")
public class PostController {

    private PostRepository repository;
    private AppUserDetailsService userService;
    private CommentRepository commentRepository;

    @Autowired
    public PostController(PostRepository repository, AppUserDetailsService userService, CommentRepository commentRepository) {
        this.repository = repository;
        this.userService = userService;
        this.commentRepository = commentRepository;
    }

    @GetMapping
    public List<Post> getPostsForUser() {
        var user = userService.loadCurrentUser();
        return repository.findAll().stream()
                .filter(post -> post.getUser().getFollowers().stream()
                        .anyMatch(follower -> follower.getId() == user.getId()))
//                .skip(startPost)
                .collect(Collectors.toList());
//        var allPosts = repository.findAll();
//        var postsForUser = new ArrayList<Post>();
//        for (Post p : allPosts) {
//            var postedBy = p.getUser();
//            var followers = postedBy.getFollowers();
//            for (AppUser follower : followers) {
//                if (follower.getId() == user.getId()) {
//                    postsForUser.add(p);
//                    break;
//                }
//            }
//        }
//        return postsForUser;
    }

    @GetMapping("/all")
    public List<Post> getPosts() {
        return repository.findAll();
    }

    @PostMapping
    public ResponseEntity addPost(@RequestBody Post post) {
        var user = userService.loadCurrentUser();
        post.setUser(user);
        repository.save(post);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/{post_id}/like")
    public ResponseEntity likePost(@PathVariable("post_id") Long postId) {
        var user = userService.loadCurrentUser();
        var post = repository.findById(postId).orElseThrow();
        post.getLikedBy().add(user);
        repository.save(post);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping("/{post_id}/unlike")
    public ResponseEntity unlikePost(@PathVariable("post_id") Long postId) {
        var user = userService.loadCurrentUser();
        var post = repository.findById(postId).orElseThrow();
        post.getLikedBy().remove(user);
        repository.save(post);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    static class CommentRequestBody {
        public String comment;
    }

    @PostMapping("/{post_id}/comment")
    public ResponseEntity addComment(@PathVariable("post_id") Long postId, @RequestBody CommentRequestBody request) {
        var user = userService.loadCurrentUser();
        var post = repository.findById(postId).get();
        var comment = new Comment();
        comment.setUser(user);
        comment.setPost(post);
        comment.setComment(request.comment);
        comment = commentRepository.save(comment);
        post.getComments().add(comment);
        repository.save(post);
        return ResponseEntity.ok().build();
    }

}
