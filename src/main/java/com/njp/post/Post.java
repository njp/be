package com.njp.post;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.njp.post.comments.Comment;
import com.njp.user.AppUser;

import javax.persistence.*;
import java.util.List;

@Entity
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String imageId;
    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "posted_by")
    private AppUser postedBy;

    @ManyToMany
    @JoinColumn(name = "liked_by")
    private List<AppUser> likedBy;

    @OneToMany
    private List<Comment> comments;

    public Post() {
    }

    public Post(String imageId, String description, AppUser user) {
        this.imageId = imageId;
        this.description = description;
        this.postedBy = user;
    }

    public Long getId() {
        return id;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AppUser getUser() {
        return postedBy;
    }

    public void setUser(AppUser user) {
        this.postedBy = user;
    }

    public List<AppUser> getLikedBy() {
        return likedBy;
    }

    public void setLikedBy(List<AppUser> likedBy) {
        this.likedBy = likedBy;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
