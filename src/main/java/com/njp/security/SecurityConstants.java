package com.njp.security;

public class SecurityConstants {

    public static final String SECRET = "peepeepoopoo";
    public static final long EXPIRATION_TIME = 864000000;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String AUTH_HEADER = "Authorization";
    public static final String SIGN_UP_URL = "/users/sign-up/**";
}
