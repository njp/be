package com.njp.user;

import com.auth0.jwt.JWT;
import com.njp.security.AppUserDetailsService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/users")
public class AppUserController {

    private AppUserRepository repository;
    private PasswordEncoder passwordEncoder;
    private AppUserDetailsService userService;
    private JavaMailSender mailSender;

    @Autowired
    public AppUserController(AppUserRepository repository, PasswordEncoder passwordEncoder, AppUserDetailsService userService, JavaMailSender mailSender) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
        this.mailSender = mailSender;
    }

    @GetMapping("/me")
    public AppUser getSelf() {
        return userService.loadCurrentUser();
    }

    @PostMapping("/sign-up")
    @CrossOrigin
    public ResponseEntity signUp(@RequestBody AppUser user) {
        if (repository.findByUsername(user.getUsername()) != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setActivated(true);
        var activationToken = UUID.randomUUID().toString();
        user.setActivationToken(activationToken);
        repository.save(user);

//        var message = new SimpleMailMessage();
//        message.setTo(user.getEmail());
//        message.setSubject("Account activation");
//        message.setText("Activate here: http://localhost:8080/users/activate/" + user.getUsername() + "/" + activationToken);
//        this.mailSender.send(message);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping
    public List<AppUser> getAllUsers() {
        return repository.findAll();
    }

    @GetMapping("/activate/{username}/{token}")
    public ResponseEntity activateUser(@PathVariable String username, @PathVariable String token) {
        var user = repository.findByUsername(username);

        if (user.getActivationToken().equals(token)) {
            user.setActivated(true);
            repository.save(user);
            return ResponseEntity.accepted().body("OK!<script>setTimeout(() => { window.location.href = 'http://localhost:4200' }, 2000)</script>");
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Failed");
        }
    }

    @GetMapping("/{user_id}")
    public AppUser getUser(@PathVariable("user_id") Long userId) {
        return repository.findById(userId).get();
    }

    @GetMapping("/{user_id}/follow")
    public ResponseEntity followUser(@PathVariable("user_id") Long userId) {
        var currentUser = userService.loadCurrentUser();
        var userToFollow = repository.findById(userId).get();
        currentUser.getFollowing().add(userToFollow);
        repository.save(currentUser);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{user_id}/unfollow")
    public ResponseEntity unfollowUser(@PathVariable("user_id") Long userId) {
        var currentUser = userService.loadCurrentUser();
        var userToFollow = repository.findById(userId).get();
        currentUser.getFollowing().remove(userToFollow);
        repository.save(currentUser);
        return ResponseEntity.ok().build();
    }
}
