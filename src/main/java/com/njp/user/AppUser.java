package com.njp.user;

import com.fasterxml.jackson.annotation.*;
import com.njp.post.Post;

import javax.persistence.*;
import java.util.List;

@Entity
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonIgnore
    private boolean activated;
    @JsonIgnore
    private String activationToken;


    private String email;

    private String username;

    private String password;

    @JoinTable(name = "gangshit",
            joinColumns = {
                @JoinColumn(
                        name = "following",
                        referencedColumnName = "id",
                        nullable = false
                )
            },
            inverseJoinColumns = {
                @JoinColumn(
                        name = "followed",
                        referencedColumnName = "id",
                        nullable = false
                )
            }
            )
    @ManyToMany
    @JsonIgnore
    private List<AppUser> following;

    @ManyToMany(mappedBy = "following")
    private List<AppUser> followers;

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    public List<AppUser> getFollowing() {
        return following;
    }

    public List<AppUser> getFollowers() {
        return followers;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getActivationToken() {
        return activationToken;
    }

    public void setActivationToken(String activationToken) {
        this.activationToken = activationToken;
    }
}
