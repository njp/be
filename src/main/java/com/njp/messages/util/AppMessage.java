package com.njp.messages.util;

public class AppMessage extends BaseMessage {
    public String from;
    public String to;
    public String message;
}
