package com.njp.messages;

import com.njp.user.AppUser;

import javax.persistence.*;

@Entity
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "sender")
    private AppUser from;

    @ManyToOne
    @JoinColumn(name = "recepient")
    private AppUser to;

    private String message;

    public Message() {
    }

    public Message(AppUser from, AppUser to, String message) {
        this.from = from;
        this.to = to;
        this.message = message;
    }

    public Long getId() {
        return id;
    }

    public AppUser getFrom() {
        return from;
    }

    public void setFrom(AppUser from) {
        this.from = from;
    }

    public AppUser getTo() {
        return to;
    }

    public void setTo(AppUser to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
