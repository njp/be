package com.njp.messages;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.google.gson.Gson;
import com.njp.messages.util.AppMessage;
import com.njp.messages.util.AuthMessage;
import com.njp.messages.util.BaseMessage;
import com.njp.security.SecurityConstants;
import com.njp.user.AppUserRepository;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.*;

public class WebSocketHandler extends TextWebSocketHandler {

    Map<String, WebSocketSession> sessions = new HashMap<>();
    Set<WebSocketSession> debug = new HashSet<>();
    private Gson gson = new Gson();

    private MessageRepository messageRepository;
    private AppUserRepository userRepository;

    public WebSocketHandler(MessageRepository messageRepository, AppUserRepository userRepository) {
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        debug.add(session);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        var typeCheck = gson.fromJson(message.getPayload(), BaseMessage.class);
        switch (typeCheck.type) {
            case "AUTH":
                var authAttempt = gson.fromJson(message.getPayload(), AuthMessage.class);
                try {
                            .build()
                            .verify(authAttempt.token.replace(SecurityConstants.TOKEN_PREFIX, ""))
                            .getSubject();
                    sessions.put(user, session);
                    System.out.println("AUTH SUCCESS");
                } catch (Exception e) {
                    session.close(CloseStatus.BAD_DATA);
                }

                break;
            case "SEND":
                var userMessage = gson.fromJson(message.getPayload(), AppMessage.class);
                try {
                    var fromSocket = session;
                    if (fromSocket != null) {
                        System.out.println("Sending from");
                        fromSocket.sendMessage(new TextMessage(
                                gson.toJson(List.of(userMessage))
                        ));
                    }
                    var toSocket = sessions.get(userMessage.to);
                    if (toSocket != null) {
                        System.out.println("Sending to");
                        toSocket.sendMessage(new TextMessage(
                                gson.toJson(List.of(userMessage))
                        ));
                    }


                    messageRepository.save(new Message(
                            userRepository.findByUsername(userMessage.from),
                            userRepository.findByUsername(userMessage.to),
                            userMessage.message
                    ));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
        System.out.println("==================================================");
        System.out.println(message.getPayload());
        System.out.println("==================================================");
    }
}
